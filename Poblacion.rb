require_relative 'Solucion'

class Poblacion
    attr_accessor :soluciones, :padres

    def initialize
        @soluciones = Array.new(99)
        @padres = []
        inicializar_soluciones
    end

    ##Función que inicializa los tableros (soluciones)
    def inicializar_soluciones
        for i in 0..soluciones.length
            soluciones[i] = Solucion.new
        end
    end

    ##Función que imprime el tablero
    def imprimir_poblacion
        soluciones.each do |solucion|
            solucion.imprimir_solucion
            puts
        end
    end

    ##Función que devuelve la solución con peor aptitud
    def recuperar_peor_aptitud
        peor_aptitud = soluciones[0]
        soluciones.each do |solucion|
            if solucion.aptitud > peor_aptitud.aptitud
                peor_aptitud = solucion
            end
        end
        peor_aptitud
    end

    ##Función que devuelve la solución con mejor aptitud
    def recuperar_mejor_aptitud
        mejor_aptitud = recuperar_peor_aptitud
        soluciones.each do |solucion|
            if solucion.aptitud < mejor_aptitud.aptitud
                mejor_aptitud = solucion
            end
        end
        mejor_aptitud
    end

    ## Recupera el tablero de la solucion
    def recuperar_solucion(index)
        soluciones[index].imprimir_tablero
    end

    def cruzar
        seleccionar_padres
        for i in 0..1
            soluciones.delete(recuperar_peor_aptitud)
        end
        i = 0
        soluciones << crear_hijos_de_padres(padres[i], padres[i+1])
        soluciones << crear_hijos_de_madres(padres[i], padres[i+1])
        padres.clear
    end

    ## Llena la lista de padres y los ordena de mejor a peor aptitud
    def seleccionar_padres
        for i in 0..4
            numero = rand(99)
            padres << soluciones[numero]
        end
        for i in 0..padres.length-1
            j=i
            while j > 0 && padres[j-1].aptitud > padres[j].aptitud
                aux = padres[j]
                padres[j] = padres[j-1]
                padres[j-1] = aux
                j = j-1
            end
        end
    end

    def crear_hijos_de_padres(padre, padre1)
        hijo = Solucion.new
        for i in 0..hijo.tablero.length - 1
            if i < hijo.tablero.length/2
                hijo.tablero[i] = padre.tablero[i]
            else
                hijo.tablero[i] = padre1.tablero[i]
            end
        end
        mutar(hijo)
        hijo.calcular_aptitud
        hijo
    end

    def crear_hijos_de_madres(madre, madre1)
        hijo = Solucion.new
        for i in 0..hijo.tablero.length - 1
            if i < hijo.tablero.length/2
                hijo.tablero[i] = madre1.tablero[i]
            else
                hijo.tablero[i] = madre.tablero[i]
            end
        end
        mutar(hijo)
        hijo.calcular_aptitud
        hijo
    end

    def mutar(solucion)
        probabilidad = rand(11)
        if probabilidad > 2
            solucion.tablero[rand(8)] = rand(8)
        end
    end

end