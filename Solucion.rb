class Solucion
    attr_accessor :tablero, :aptitud

    ## Función que inicializa el tablero
    def initialize
        @tablero = Array.new(7)
        @aptitud = 0
        poblar_solucion
        calcular_aptitud
    end

    ## Función que llena el tablero, posicionando las reinas
    def poblar_solucion
        for i in 0..tablero.length
            tablero[i] = rand(8)
        end
    end

    def calcular_aptitud
        @aptitud = 0
        for index in 0..7
            amenazas_vertical = encontrar_amenazas_inferior_vertical(index)
            amenazas_diagonal_izquierda = encontrar_amenazas_inferior_diagonal_izquierda(index)
            amenazas_diagonal_derecha = encontrar_amenazas_inferior_diagonal_derecha(index)
            @aptitud += amenazas_vertical + amenazas_diagonal_derecha + amenazas_diagonal_izquierda
        end
    end

    ##Función que detecta el número total de amenazas en vertical inferior de una reina
    def encontrar_amenazas_inferior_vertical(index)
        amenazas = 0
        for j in index+1..7
            if tablero[j] == tablero[index]
                amenazas += 1
                break
            end
        end        
        amenazas
    end

    ##Función que detecta el número total de amenazas en diagonal inferior derecha de una reina
    def encontrar_amenazas_inferior_diagonal_derecha(index)
        amenazas = 0
        casillas_recorridas = 1
        for j in index+1..7
            if tablero[index] == tablero[j] - casillas_recorridas
                amenazas += 1
                break
            end
            casillas_recorridas += 1
        end
        amenazas
    end
    
    ##Función que detecta el número total de amenazas en diagonal inferior izquierda de una reina
    def encontrar_amenazas_inferior_diagonal_izquierda(index)
        amenazas = 0
        casillas_recorridas = 1
        for j in index+1..7
            if tablero[index] == tablero[j] + casillas_recorridas
                amenazas += 1
                break
            end
            casillas_recorridas += 1
        end
        amenazas
    end

    ##Función que imprime la solución
    def imprimir_solucion
        for i in 0..tablero.length
            print tablero[i]
        end
        puts
    end

    ##Función que facilita la visualización del tablero
    def imprimir_tablero
        for i in 0..7
            for j in 0..7
                if j == tablero[i]
                    print 1
                    print " "
                else
                    print 0
                    print " "
                end
            end
            puts
        end
    end
end