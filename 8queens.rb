require_relative 'Solucion'
require_relative 'Poblacion'

poblacion = Poblacion.new

i = 0
resuelto = false

while i < 10000
    mejor_aptitud = poblacion.recuperar_mejor_aptitud
    if mejor_aptitud.aptitud == 0
        puts "Veces repetido: #{i}"
        mejor_aptitud.imprimir_solucion
        mejor_aptitud.imprimir_tablero
        puts "Aptitud: #{mejor_aptitud.aptitud}"
        break
    end
    poblacion.cruzar
    i += 1
end